﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1, 4, 4, 3, 2 });

        Console.WriteLine(sorted);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        Array.Sort(x);
        Array.Reverse(x);
        for (int i = 0; i < x.Length; i++)
        {
            Console.WriteLine(x[i] + "");
        }
        return x;
    }
}