﻿var result = PracticeStrings.MixTwoStrings("", "");
Console.WriteLine(result);
Console.WriteLine("hello world");

public class PracticeStrings
{
    public static string MixTwoStrings(string value1, string value2)
    {
        if (string.IsNullOrEmpty(value1) && string.IsNullOrEmpty(value2)) return string.Empty;
        if (!string.IsNullOrEmpty(value1) && string.IsNullOrEmpty(value2)) return value1;
        if (string.IsNullOrEmpty(value1) && !string.IsNullOrEmpty(value2)) return value2;
        string result = null;
        char[] arrayChar1 = value1.ToCharArray();
        char[] arrayChar2 = value2.ToCharArray();
        for (int i = 0; i < arrayChar2.Length; i++)
        {
            while (i < arrayChar1.Length)
            {
                result += arrayChar1[i].ToString() + arrayChar2[i].ToString();
                break;
            }
            if (i >= arrayChar1.Length)
            {
                result += arrayChar2[i].ToString();
            }
        }
        return result;
    }
}

